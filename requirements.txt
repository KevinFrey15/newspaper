asgiref==3.4.1
beautifulsoup4==4.10.0
Django==4.0
django-bootstrap-v5==1.0.7
django-environ==0.8.1
psycopg2-binary==2.9.2
soupsieve==2.3.1
sqlparse==0.4.2
